#!/usr/bin/python

import matplotlib.pyplot as plt
import sys
import numpy as np
import os
import pandas as pd


def prepare_figure_for_publication(ax=None,
        width_cm=None,
        width_inches=None,
                                   height_cm=None,
        height_inches=None,
                                   fontsize=None,
        fontsize_labels=None,
        fontsize_ticklabels=None,
        fontsize_legend=None,
                                   fontsize_annotations =None,
                                   TeX = True, # Used for ax=None case (setup)
        ):

    """
    Two ways to use this:
    (1) Before creating a figure, with ax=None
    (2) To fine-tune a figure, using ax

    One reasonable option for making compact figures like for Science/Nature is to create everything at double scale. 
    This works a little more naturally with Matplotlib's default line/axis/etc sizes.

    Also, if you change sizes of, e.g. xticklabels and x-axis labels after they've been created, they will not necessarily be relocated appropriately.
    So  you can call prepare_figure_for_publication with no ax/fig argument to set up figure defaults
    prior to creating the figure in the first place.


Some wisdom on graphics:
 - 2015: How to produce PDFs of a given width, with chosen font size, etc:
   (1) Fix width to journal specifications from the beginning / early. Adjust height as you go, according to preferences for aspect ratio:
    figure(figsize=(11.4/2.54, chosen height))
   (2) Do not use 'bbox_inches="tight"' in savefig('fn.pdf').  Instead, use the subplot_adjust options to manually adjust edges to get the figure content to fit in the PDF output
   (3) Be satisfied with that. If you must get something exactly tight and exactly the right size, you do this in Inkscape. But you cannot scale the content and bbox in the same step. Load PDF, select all, choose the units in the box at the top of the main menu bar, click on the lock htere, set the width.  Then, in File Properties dialog, resize file to content. Save.

    """
    if ax is None: # Set up plot settings, prior to creation fo a figure
        params = { 'axes.labelsize': fontsize_labels if fontsize_labels is not None else fontsize,
                   'font.size': fontsize,
                   'legend.fontsize': fontsize_legend if fontsize_legend is not None else fontsize,
                   'xtick.labelsize': fontsize_ticklabels if fontsize_ticklabels is not None else fontsize_labels if fontsize_labels is not None else fontsize,
                   'ytick.labelsize': fontsize_ticklabels if fontsize_ticklabels is not None else fontsize_labels if fontsize_labels is not None else fontsize,
                   'figure.figsize': (width_inches, height_inches),
            }
        if TeX:
            params.update({
                   'text.usetex': TeX,
                       'text.latex.preamble': r'\usepackage{amsmath} \usepackage{amssymb}',
                'text.latex.unicode': True,
            })
        if not TeX:
            params.update({'text.latex.preamble':''})

        plt.rcParams.update(params)
        return
    
    fig = ax.get_figure() 
    if width_inches:
        fig.set_figwidth(width_inches)
        assert width_cm is None
    if height_inches:
        fig.set_figheight(height_inches)
        assert height_cm is None
    if width_cm:
        fig.set_figwidth(width_cm/2.54)
        assert width_inches is None
    if height_cm:
        fig.set_figheight(height_cm/2.54)
        assert height_inches is None
    
    #ax = plt.subplot(111, xlabel='x', ylabel='y', title='title')
    for item in fig.findobj(plt.Text) + [ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels():
        if fontsize:
            item.set_fontsize(fontsize)

def plot_diagonal(xdata=None, ydata=None, ax=None, **args):
    """ Plot a 45-degree line 
    """
    if ax is None: ax = plt.gca()
    #LL = min(min(df[xv]), min(df[yv])), max(max(df[xv]), max(df[yv]))
    if xdata is None and ydata is None:
        xl, yl = ax.get_xlim(), ax.get_ylim()
        LL = max(min(xl), min(yl)),    min(max(xl), max(yl)),
    elif xdata is not None and ydata is None:
        assert isinstance(xdata, pd.DataFrame)
        dd = xdata.dropna()
        LL = dd.min().max(), dd.max().min()
    else:
        assert xdata is not None
        assert ydata is not None
        #if isinstance(xdata, pd.Series): xdata = xdata.vlu
        xl, yl = xdata, ydata
        LL = max(min(xl), min(yl)),    min(max(xl), max(yl)),
    ax.plot(LL, LL,  **args)

                        
def figureFontSetup(uniform=12,figsize='paper', amsmath=True):
    """
This is deprecated. Use prepare_figure_for_publication


Set font size settings for matplotlib figures so that they are reasonable for exporting to PDF to use in publications / presentations..... [different!]
If not for paper, this is not yet useful.


Here are some good sizes for paper:
    figure(468,figsize=(4.6,2)) # in inches
    figureFontSetup(uniform=12) # 12 pt font

    for a subplot(211)

or for a single plot (?)
figure(127,figsize=(4.6,4)) # in inches.  Only works if figure is not open from last run!
        
why does the following not work to deal with the bad bounding-box size problem?!
inkscape -f GSSseries-happyLife-QC-bw.pdf --verb=FitCanvasToDrawing -A tmp.pdf .: Due to inkscape cli sucks! bug.
--> See savefigall for an inkscape implementation.

2012 May: new matplotlib has tight_layout(). But it rejigs all subplots etc.  My inkscape solution is much better, since it doesn't change the layout. Hoewever, it does mean that the original size is not respected! ... Still, my favourite way from now on to make figures is to append the font size setting to the name, ie to make one for a given intended final size, and to do no resaling in LaTeX.  Use tight_layout() if it looks okay, but the inkscape solution in general.
n.b.  a clf() erases size settings on a figure! 

    """
    figsizelookup={'paper':(4.6,4),'quarter':(1.25,1) ,None:None}
    try:
        figsize=figsizelookup[figsize]
    except KeyError as TypeError:
        pass
    params = {#'backend': 'ps',
           'axes.labelsize': 16,
        #'text.fontsize': 14,
        'font.size': 14,
           'legend.fontsize': 10,
           'xtick.labelsize': 16,
           'ytick.labelsize': 16,
           'text.usetex': True,
           'figure.figsize': figsize
        }
           #'figure.figsize': fig_size}
    if uniform is not None:
        assert isinstance(uniform,int)
        params = {#'backend': 'ps',
           'axes.labelsize': uniform,
            #'text.fontsize': uniform,
           'font.size': uniform,
           'legend.fontsize': uniform,
           'xtick.labelsize': uniform,
           'ytick.labelsize': uniform,
           'text.usetex': True,
           'text.latex.unicode': True,
            'text.latex.preamble':r'\usepackage{amsmath},\usepackage{amssymb}',
           'figure.figsize': figsize
           }
        if not amsmath:
            params.update({'text.latex.preamble':''})

    plt.rcParams.update(params)
    plt.rcParams['text.latex.unicode']=True
    #if figsize:
    #    plt.rcParams[figure.figsize]={'paper':(4.6,4)}[figsize]

    return(params)

# Nothing fancy here, but reminder how to use the "ma" keyword and xycoords:
def bottomlefttext(ax,text, size=12, alpha = 0.5, color='k', dx=1.0/20, dy=1.0/20, edgecolor = "0.5"):
    return  ax.annotate(text, xy=(dx, dy), xycoords = 'axes fraction', ha='left', ma='left', va='bottom', color=color, size=size,  bbox=dict(boxstyle="round", fc="w", ec=edgecolor, alpha=alpha))
def bottomrighttext(ax,text, size=12, alpha = 0.5, color='k', dx=1.0/20, dy=1.0/20, edgecolor = "0.5"):
    return  ax.annotate(text, xy=(1-dx, dy), xycoords = 'axes fraction', ha='right', ma='left', va='bottom', color=color, size=size,  bbox=dict(boxstyle="round", fc="w", ec=edgecolor, alpha=alpha))
def toplefttext(ax,text, size=12, alpha = 0.5, color='k', dx=1.0/20, dy=1.0/20, frameon=True, edgecolor = "0.5"):
    return  ax.annotate(text, xy=(dx, 1-dy), xycoords = 'axes fraction', ha='left', ma='left', va='top', color=color, size=size,  bbox= None if not frameon else dict(boxstyle="round", fc="w", ec=edgecolor, alpha=alpha))
def toprighttext(ax,text, size=12, alpha = 0.5, color='k', dx=1.0/20, dy=1.0/20, edgecolor = "0.5"):
    return  ax.annotate(text, xy=(1-dx, 1-dy), xycoords = 'axes fraction', ha='right', ma='left', va='top', color=color, size=size,  bbox=dict(boxstyle="round", fc="w", ec=edgecolor, alpha=alpha))

def vtext(xvec,  yvec, tvec, ax=None, **args):
    """ Why does ax.text only take scalars?
 This is a vectorized version
"""
    # Still to do: convert from Series, etc:
    assert len(xvec)== len(yvec) ==len(tvec)
    xvec=xvec.values
    yvec=yvec.values
    tvec=tvec.values
    cvec = None
    if 'color' in args:
        if len(args['color']) == len(xvec):
               cvec = args.pop('color')
    
    for ii,tt in enumerate(tvec):
        if cvec is None:
            ax.text(xvec[ii], yvec[ii], tt, **args)
        else:
            ax.text(xvec[ii], yvec[ii], tt, color = cvec[ii], **args)
   



def test_bug3_for_multipage_plot_iterator():
    import numpy as np
    import matplotlib.pyplot as plt

    def prep_figure():
        plt.close('all')
        fig, axs = plt.subplots(4, 3, figsize=(11, 8.5))
        axs = np.concatenate(axs)
        for ii in range(5):
            axs[ii].plot([1, 2, 3], [-10, -1, -10])
            axs[ii].set_ylabel('ylabel')
            axs[ii].set_xlabel('xlabel')
        return fig, axs

    fig, axs = prep_figure()
    plt.tight_layout()
    plt.show()
    plt.savefig('tmp.pdf', )

    # Try deleting extra axes
    fig, axs = prep_figure()
    for ii in range(5, 12):
        fig.delaxes(axs[ii])
    plt.tight_layout()
    plt.draw()
    plt.savefig('tmpd.pdf', )

    # Try hiding  extra axes
    fig, axs = prep_figure()
    for ii in range(5, 12):
        axs[ii].set_visible(False)
    plt.tight_layout()
    plt.draw()
    plt.savefig('tmph.pdf', )


def multipage_plot_iterator(items, nrows=None, ncols=None, filename=None, wh_inches=None, transparent=True,):
    """
    If you want to have a series of subplots that goes more than one page,  use this to generate figs and axes handles.
    You specify the list of data items which you will use to plot in each axis, how many (rows and columns) to plot per page, and the filename stem for the pages (which will ultimately be a single multi-page PDF).
    In addition, you can specify some other stuff about the layout.
    Then this iterator will return series of item, fig and axis values for each item in the list (e.g data for one country per axis), as well as whether each axis is on the right/left/bottom of the page.

    In order to be able to finish up by concatenating the PDFs, etc, one extra "yield" will be made. This simply duplicates the final axis/plot. Therefore, do not use your own counters in loops over this generator. Instead, build any counters into the
    "items" data structure.

    items could be an iterator itself, but that is not implemented yet. It must be a list at the moment.

    transparent [True]:  Set this to False if you use  ax.set_facecolor in your loop; this will avoid using transparency in the saved result.

    Deletes the one-page files after creating the final merged-page produce

    To do:
     - check that this also works nicely for ncols==nrows==1
     - Rewrite so "items" can be a groupby object or other iterable.
    """
    from .utilities import str2pathname, mergePDFs
    if str2pathname(filename, check=True, includes_path=True):
        print((' WARNING ({}):  Modifying filename to clean out certain characters'.format(
            sys._getframe().f_code.co_name)))
        fn0 = filename
        # Proceed anyway.
        filename = str2pathname(filename, includes_path=True)
        print(('   FROM {} \n    --> {}'.format(fn0, filename)))
    nItems = len(items)
    if wh_inches is None:
        wh_inches = [8.75, 7]  # for full-page figures
    elif wh_inches == "landscape":
        wh_inches = [8.75, 7]  # for full-page figures
    elif wh_inches == "portrait":
        wh_inches = [7, 8.75]  # for full-page figures
    ncols = 4 if ncols is None else ncols
    nrows = 3 if nrows is None else nrows
    splotNums = [(ii*nrows*ncols, min(nItems, (ii+1)*nrows*ncols))
                 for ii in range(int(np.ceil(nItems/nrows/ncols)))]
    nPages = len(splotNums)
    figureFontSetup(uniform=9)
    pagefiles = []
    for ipage, (ssplot, esplot) in enumerate(splotNums):
        if ipage % 10 == 0:
            plt.close('all')
        # If it's the last page, possibly adjust figure height:
        if 0:
            # less than nrows when the page isn't full
            actualRows = int(np.ceil((esplot-ssplot-1)*1.0/ncols))
            print(wh_inches)
            wh_inches[1] = wh_inches[1] * actualRows/nrows
            print(wh_inches)
        else:
            actualRows = nrows

        fig, axs = plt.subplots(actualRows, ncols, figsize=wh_inches[::-1])
        try:  # Normally, there is more than one axis:
            axs = list(np.concatenate(axs))
        except TypeError as err:
            axs = [axs]
        # If we don't need them all, erase some. This allows for us to call layout_tight() later, though so far it doesn't drop the whitespace.
        for idelAx in np.arange(esplot-ssplot, len(axs)):
            axs[idelAx].plot(1, 1)
            # fig.delaxes(axs[idelAx])
            # axs[idelAx].set_visible(False) # This is not the same as deleting them
        for iItem, anitem in enumerate(items[ssplot:esplot]):
            ax = axs[iItem]

            yield(dict(data=anitem, ax=ax, fig=fig, bottom=iItem > esplot-ssplot-ncols-1,  # iItem>=(nrows-1)*ncols ,
                       left=not (iItem) % ncols, first=iItem == 0, last=iItem == esplot-ssplot, ipage=ipage))

            # yield(dict(data = anitem, ax = ax, fig = fig, bottom = iItem > erow-srow-ncols-1, #iItem>=(nrows-1)*ncols ,
            #           left = not (iItem)%ncols, first = iItem==0, last = iItem == erow-srow-1 , ipage =ipage))

        plt.tight_layout()
        for idelAx in np.arange(esplot-ssplot, len(axs)):
            # fig.delaxes(axs[idelAx])
            axs[idelAx].set_visible(False)
        pagefilename = filename+'page%02d' % ipage
        pagefiles += [pagefilename+'.pdf']
        plt.savefig(pagefilename+'.pdf',  wh_inches=wh_inches,)
        # It seems something in the following is no good (for grid cell images) so just use plain savefig
        #savefigall(pagefilename,  wh_inches=wh_inches,
        #           rv=False, png=False, transparent=transparent)
    mergePDFs(pagefiles, filename+'ALL.pdf')
    # This allows a final "next" by the caller to finish the final saving.
    yield (dict(data=anitem, ax=ax, fig=fig, bottom=iItem >= (actualRows-1)*ncols, left=not (iItem) % ncols, first=iItem == 0, last=iItem == esplot-ssplot, ipage=ipage))
    # yield (dict(data = anitem, ax = ax, fig = fig, bottom = iItem>=(nrows-1)*ncols, left = not (iItem)%ncols, first = iItem==0, last = iItem == erow-srow-1 , ipage =ipage)) # This allows a final "next" by the caller to finish the final saving.

        
